package mx.com.zara.prices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;


/**
 * 
 * Clase
 * principal
 * proyecto tienda 
 *
 */
@OpenAPIDefinition(info = @Info(title = "Princes API", version= "1.0", description = "API para busqueda de precios"))
@SpringBootApplication
public class ShopApplication {

	/**
	 * 
	 * Metodo principal de Spring Boot y
	 * @param args parametros de entrada
	 */
	public static void main(String[] args) {
		SpringApplication.run(ShopApplication.class, args);
	}

}
