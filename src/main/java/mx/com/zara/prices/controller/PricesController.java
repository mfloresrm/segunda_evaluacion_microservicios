/**
 * 
 */
package mx.com.zara.prices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import mx.com.zara.prices.model.PricesResponse;
import mx.com.zara.prices.service.PricesService;

/**
 * @author Michael
 *
 */
/*
 * Controlador de API
 */
@Slf4j
@RestController
public class PricesController {
	
	/*
	 * Inyeccion de PricesServices
	 */
	@Autowired
	private PricesService pricesService;
	
	/*
	 * Método GetMapping
	 */
	@Operation(summary = "Get prices of a product filtering by id product, id brand and application date")
	@ApiResponses(value = {	
		@ApiResponse(responseCode = "400", description="Invalid request supplied"),
		@ApiResponse(responseCode = "404", description = "Price not found")
	}	)
	/*
	 * URI /prices
	 */
	/**
	 * getPricesByIdProductAndIdBrandAndApplicationDate
	 * @param <idProduct> Identificador de producto
	 * @param <idMarca> Identificador de marca
	 * @param <AplicationDate>  Fecha de aplicacion
	 * @return
	 */
	@GetMapping("/prices")
	public ResponseEntity<PricesResponse> getPricesByIdProductAndIdBrandAndApplicationDate(
			@RequestParam String idProduct, 
			@RequestParam String idMarca, 
			@RequestParam String aplicationDate) {
		/*
		 * Log consulta
		 */
		log.info("Consulta de precios con filtros idProduct: {}", idProduct);

		/*
		 * Instancia de PricesResponse
		 */
		PricesResponse pricesResponse = pricesService.getPricesByIdProductAndIdBrandAndApplicationDate(idProduct,
				idMarca, aplicationDate);
		/*
		 * Retorno de entidad
		 */
		return new ResponseEntity<>(pricesResponse, HttpStatus.OK);
	}

}
