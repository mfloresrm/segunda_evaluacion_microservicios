/**
 * 
 */
package mx.com.zara.prices.model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.NoArgsConstructor;

/**
 * @author Michael
 *
 */
@NoArgsConstructor
public class PricesResponse {
	/*
	 * Atributo de product_id
	 */
	private int idProducto;
	/*
	 * Atributo de brand_id
	 */
	private int idMarca;
	/*
	 * Atributo para asignar tarifa
	 */
	private int tarifa;
	
	/*
	 * Atributos para fecha de inicio y fin de tarifa
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date fechaInicio; 
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date fechaTermino; 
	/*
	 * Precio final de la venta
	 */
	private BigDecimal precio;
	/**
	 * @return the idProducto
	 */
	public int getIdProducto() {
		return idProducto;
	}
	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	/**
	 * @return the idMarca
	 */
	public int getIdMarca() {
		return idMarca;
	}
	/**
	 * @param idMarca the idMarca to set
	 */
	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}
	/**
	 * @return the tarifa
	 */
	public int getTarifa() {
		return tarifa;
	}
	/**
	 * @param tarifa the tarifa to set
	 */
	public void setTarifa(int tarifa) {
		this.tarifa = tarifa;
	}
	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return (Date) fechaInicio.clone();
	}
	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = (Date) fechaInicio.clone();
	}
	/**
	 * @return the fechaTermino
	 */
	public Date getFechaTermino() {
		return (Date) fechaTermino.clone();
	}
	/**
	 * @param fechaTermino the fechaTermino to set
	 */
	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = (Date) fechaTermino.clone();
	}
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	} 

}
