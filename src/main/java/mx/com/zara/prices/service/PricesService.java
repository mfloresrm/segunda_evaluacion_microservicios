/**
 * 
 */
package mx.com.zara.prices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.zara.prices.entities.PricesEntity;
import mx.com.zara.prices.model.PricesResponse;
import mx.com.zara.prices.repository.PricesRepository;
/**
 * @author Michael
 *
 */
@Slf4j
@Service
public class PricesService {
	
	/*
	 * Inyeccion de PricesRepository
	 */
	/** 
	 *  Clase PricesRepository
	 *  inyectado en PricesServices
	 *  
	 */
	@Autowired 
	public PricesRepository pricesRepository;
	
	/**
	 * getPricesByIdProductAndIdBrandAndApplicationDate
	 * 
	 * @param <aplicationDate> Fecha de aplicacion
	 * @param <idMarca> Identificador de la marca
	 * @param <idProduct> Identificado del producto
	 * @return Retorno de instancia PrincesResponse 
	 */
	public PricesResponse getPricesByIdProductAndIdBrandAndApplicationDate(
			String idProduct,
			String idMarca,
			String aplicationDate) {
		
		log.info("Inicia llamada a componente externo webservice");
		PricesResponse pricesResponse = null;
		PricesEntity pricesEntity = pricesRepository.findPricesByIdProductAndBrand(aplicationDate, idMarca, idProduct);
		/*
		 * Respuesta nula
		 */
		if (pricesEntity != null) {		
			log.error("No encontre valores en la base de datos y devolvere una respuesta vacia Filtros utilizados {} : {} : {} ", idProduct, idMarca, aplicationDate);
			pricesResponse = convertEntity2DTO(pricesEntity);
		} 
		/*
		 * Retorno de instancia
		 */
		return pricesResponse;
	}

	/**
	 * convertEntity2DTO
	 * @param pricesEntity
	 * @return
	 */
	private PricesResponse convertEntity2DTO(PricesEntity pricesEntity) {
		/*
		 * Creacion de instancia de la clase PricesResponse
		 */
		PricesResponse pricesResponse = new PricesResponse();
		
		/*
		 * Asignacion de valores
		 */
		try {
			pricesResponse.setFechaInicio(pricesEntity.getStartDate());
			pricesResponse.setFechaTermino(pricesEntity.getEndDate());
			pricesResponse.setIdMarca(pricesEntity.getIdBrand());
			pricesResponse.setIdProducto(pricesEntity.getProductId());
			pricesResponse.setPrecio(pricesEntity.getPrice());
			pricesResponse.setTarifa(pricesEntity.getPriceList());

		} catch (RuntimeException e) {
			/*
			 * Excepcion para un valor nulo
			 */
			log.info("Valor nulo", e);
		}
		/*
		 * Retorno de instancia
		 */
		return pricesResponse;
	}
	
	/*
	 * Metodo para prueba test
	 */
	/** 
	 *  Metodo para realizar
	 *  un test unitario
	 * @param value valor que se va a evaluar
	 * @return retorno de resultado a evaluar
	 */
	public Boolean isNumerber(String value){
		boolean resul;
	    try {
            Integer.parseInt(value);
            resul = true;
        } catch (NumberFormatException excepcion) {
        	resul = false;
        }
		
		return resul;
		
	}

}
