package mx.com.zara.prices;



import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class ShopApplicationTests {
	

	ShopApplication shop;
	
	@BeforeEach
	void configuracion() {
		shop  = new ShopApplication();
	}
	
	@Test
	void shopNotNullTest() {
		assertNotNull(shop);
		ShopApplication.main(new String[] {});
	}
	
	@AfterEach
	void liberar() {
		shop=null;
	}


}
