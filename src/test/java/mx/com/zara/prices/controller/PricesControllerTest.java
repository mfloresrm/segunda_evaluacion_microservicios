package mx.com.zara.prices.controller;

import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import mx.com.zara.prices.service.PricesService;


@WebMvcTest(PricesController.class)
class PricesControllerTest {
	
	@Autowired
	private MockMvc mock;
	
	@MockBean
	private PricesService pricesService;
	
	@Test
	void uriTest() throws Exception {
		when(pricesService
				.getPricesByIdProductAndIdBrandAndApplicationDate("", "", ""))
				.thenReturn(null);
	}
	
	@Test
	void uriTest2() throws Exception {
		mock.perform(MockMvcRequestBuilders.
				get("/prices").
				accept(MediaType.APPLICATION_JSON));
	}

}
