package mx.com.zara.prices.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.meanbean.test.BeanTester;

class PricesResponseTest {
	PricesResponse obj = new PricesResponse();
	  @Test
	  void testClass() {
		  new BeanTester().testBean(PricesResponse.class);
	  }
	  
	  @BeforeEach
	  void configuracion() {
		obj.setIdProducto(1);
		obj.setIdMarca(255);
		obj.setTarifa(6);
		obj.setFechaInicio(new GregorianCalendar(2022, Calendar.JUNE, 15).getTime());
		obj.setFechaTermino(new GregorianCalendar(2022, Calendar.JUNE, 16).getTime());
		obj.setPrecio(new BigDecimal("35.52"));
	  }

	  @Test
	  void Test() {
		  assertNotNull(obj.getIdProducto());
		  assertNotNull(obj.getIdMarca());
		  assertNotNull(obj.getTarifa());
		  assertNotNull(obj.getFechaInicio());
		  assertNotNull(obj.getFechaTermino());
		  assertNotNull(obj.getPrecio());
	  }
}
