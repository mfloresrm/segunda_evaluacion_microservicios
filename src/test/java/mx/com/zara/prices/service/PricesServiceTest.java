package mx.com.zara.prices.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import mx.com.zara.prices.entities.PricesEntity;
import mx.com.zara.prices.repository.PricesRepository;

class PricesServiceTest {
	PricesEntity pricesEntity;

	PricesService pricesServices;

	@BeforeEach
	void setUp() {
	  pricesServices = new PricesService();

	  pricesServices.pricesRepository = Mockito.mock(PricesRepository.class);
	  pricesEntity = new PricesEntity();
	  pricesEntity.setIdBrand(1);
	  pricesEntity.setPriceList(5);
	  pricesEntity.setStartDate(new GregorianCalendar(2022, Calendar.JUNE, 14).getTime());
	  pricesEntity.setEndDate(new GregorianCalendar(2022, Calendar.JUNE, 15).getTime());
	  pricesEntity.setProductId(35455);
	  pricesEntity.setPriority(1);
	  pricesEntity.setPrice(new BigDecimal("35.50"));
	  pricesEntity.setCurrency("EUR");
	}

	@Test
	void getPricesByIdProductAndIdBrandAndApplicationDateTest() {

	  when(pricesServices.pricesRepository.findPricesByIdProductAndBrand("", "", "")).thenReturn(pricesEntity);

	  pricesServices.getPricesByIdProductAndIdBrandAndApplicationDate("", "", "");
	}

	@Test
	void isNumerberTest() {

	  assertEquals(pricesServices.isNumerber("67"), true);
	}

	@Test
	void isNumerberTestNot() {
	  pricesServices.isNumerber("palabra");
	}
}
